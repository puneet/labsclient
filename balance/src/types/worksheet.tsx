export interface IWorksheet {
  id: string;
  name: string;
  status: number;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: string;
  version: number;

  samples: ISample[];
  standards: IStandard[];

  chemicals: IChemical[];
  solvents: ISolvent[];
  equipments: IEquipment[];

  internalStandardSolution: IInternalStandardSolution;
}

export interface IVolumeMeasurement {
  value: number;
  unitId: number;
}

export interface IWeightMeasurement {
  value: number;
  unitId: number;
}

export interface IReplicate {
  id: string;
  sequenceNumber: number;
  name?: string;
  replicateWeight?: IWeightMeasurement;
  istdWeight?: IWeightMeasurement;
  theoreticalVolume?: IVolumeMeasurement;
  dilution?: IVolumeMeasurement;
  isRejected: boolean;
  rejectionReasonId?: number | null;
  rejectionReasonText?: string | null;
  weighedBy?: string;
  weighedOn?: Date;
  weightTransferredFromBalance?: boolean;
  rejectedBy?: string | null;
  rejectedOn?: Date | null;
}

export interface ISample {
  id: string;
  barcode: string;
  name: string;
  replicates: IReplicate[];
}

export interface IStandard {
  barcode: string;
  name: string;
  replicates: IReplicate[];
}

export interface IStandardLimsInfo {
  identity: string;
  name: string;
  barcode: string;
  batchCode: string;
  Purity: number;
  PurityUom: string;
  Expiry: Date;
  ExpiryAms: string;
}

export interface ISampleLimsInfo {
  requestId: string;
  rcmNumber: number;
  projectId: string;
  experimentId: string;
  taskId: string;
  glpStudyNo: string;
  materialId: string;
  materialName: string;
  batchCode: string;
  barcode: number;
  Analysis: string;
  Comments: string;
  Requestor: string;
  RequestCreated: Date;
}

export interface IChemical {
  id: string;
  name: string;
}

export interface ISolvent {
  id: string;
  chemicalName: string;
}

export interface IEquipment {
  id: string;
  barcode: string;
  name: string;
}

export interface IInternalStandard {
  id: string;
  chemicalName: string;
  manufacturer: string;
  lotNumber: string;
}

export interface IInternalStandardSolution {
  id: string;
  datePrepared: Date;
  reference: string;
  internalStandard: IInternalStandard;
  solvents: ISolvent[];
  weight: number;
  volume: number;
  balance: number | null;
}
