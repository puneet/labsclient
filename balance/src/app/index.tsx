import React, { useEffect } from "react";
import Routes from "./routes";
import { connect } from "react-redux";
import { fetchLookups } from "./dux/actions";

function App(props: any) {
	useEffect(props.fetchLookups, []);

	if (
		!props.weightUnits &&
		!props.volumeUnits &&
		!props.weightUnits.length &&
		!props.volumeUnits.length
	) {
		return null;
	}

	return <Routes />;
}

function mapStateToProps(state: any) {
	const { weightUnits, vlumeUnits } = state.appState;
	return {
		weightUnits,
		vlumeUnits
	};
}

const mapDispatchToProps = {
	fetchLookups
};

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export default ConnectedApp;
