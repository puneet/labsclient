import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Worksheets from "./../worksheets";
import ViewWorksheet from "./../worksheets/readonly";
import EditWorksheet from "./../worksheets/editable";

function Routes(props: any) {
	return (
		<Router>
			<Switch>
				<Route exact path="/" component={Worksheets} />
				<Route
					exact
					path="/worksheets/:worksheetId/view"
					component={ViewWorksheet}
				/>
				<Route
					path="/worksheets/:worksheetId/edit"
					component={EditWorksheet}
					{...props}
				/>
			</Switch>
		</Router>
	);
}

export default Routes;
