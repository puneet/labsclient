import axios from "axios";

export function fetchLookups() {
  return (dispatch: any) => {
    dispatch({
      type: "FETCH_WEIGHT_UNITS",
      payload: axios.get("/weight-units.json"),
    });
    dispatch({
      type: "FETCH_VOLUME_UNITS",
      payload: axios.get("/volume-units.json"),
    });
    dispatch({
      type: "FETCH_REJECTION_REASONS",
      payload: axios.get("/rejection-reasons.json"),
    });
  };
}
