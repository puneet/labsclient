export interface ILookup {
  id: number;
  name: string;
}

interface IAppState {
  weightUnits: ILookup[];
  volumeUnits: ILookup[];
  rejectionReasons: ILookup[];
}

const defaultState: IAppState = {
  weightUnits: [],
  volumeUnits: [],
  rejectionReasons: [],
};

const reducer = (state: IAppState = defaultState, action: any = {}) => {
  switch (action.type) {
    case "FETCH_WEIGHT_UNITS_FULFILLED": {
      return { ...state, weightUnits: action.payload.data };
    }

    case "FETCH_VOLUME_UNITS_FULFILLED": {
      return { ...state, volumeUnits: action.payload.data };
    }

    case "FETCH_REJECTION_REASONS": {
      return { ...state, rejectionReasons: action.payload.data };
    }

    default:
      return state;
  }
};

export default reducer;
