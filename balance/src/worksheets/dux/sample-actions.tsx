export function addReplicate(worksheetId: string, sampleId: string) {
  return (dispatch: any) => {
    dispatch({
      type: "ADD_SAMPLE_REPLICATE",
      payload: { worksheetId, sampleId },
    });
  };
}

export function rejectReplicate(
  worksheetId: string,
  sampleId: string,
  replicateId: string
) {
  return (dispatch: any) => {
    dispatch({
      type: "REJECT_SAMPLE_REPLICATE",
      payload: { worksheetId, sampleId, replicateId },
    });
  };
}
