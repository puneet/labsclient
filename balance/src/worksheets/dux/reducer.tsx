import { IWorksheet, ISample, IReplicate } from "./../../types/worksheet";
import uuidv4 from "../../shared/uuid";

interface IWorksheetState {
  worksheets: IWorksheet[];
  worksheet: IWorksheet | null;
}

const defaultState: IWorksheetState = {
  worksheets: [],
  worksheet: null,
};

const reducer = (state: IWorksheetState = defaultState, action: any = {}) => {
  switch (action.type) {
    case "FETCH_WORKSHEETS_FULFILLED": {
      return { ...state, worksheets: action.payload.data };
    }

    case "FETCH_WORKSHEET_FULFILLED": {
      return { ...state, worksheet: action.payload.data };
    }

    case "CLEAR_WORKSHEET": {
      return { ...state, worksheet: null };
    }

    case "ADD_SAMPLE_REPLICATE": {
      //@@@ Should we have sample Id instead of using the barcode here?
      const { worksheetId, sampleId } = action.payload;
      let { worksheet } = state;
      let sample = findSampleByBarcode(state, worksheetId, sampleId);
      if (sample) {
        const maxSequenceNumber = findMaxSequenceNumber(sample.replicates);
        sample.replicates = [
          ...sample.replicates,
          {
            id: uuidv4(),
            sequenceNumber: maxSequenceNumber + 1,
            isRejected: false,
          },
        ];
      }

      return { ...state, worksheet: { ...worksheet } };
    }

    default:
      return state;
  }
};

function findSampleByBarcode(
  state: IWorksheetState,
  worksheetId: string,
  sampleId: string
): ISample | null {
  //@@@ Should we have sample Id instead of using the barcode here?
  let sample = null;
  let { worksheet } = state;
  if (worksheet) {
    let samples = worksheet?.samples || [];
    sample = samples.find((sample) => sample.barcode === sampleId);
  }

  return sample || null;
}

function findMaxSequenceNumber(replicates: IReplicate[]): number {
  const replicateWithMaxSequenceNumber = replicates.reduce((prev, current) =>
    prev.sequenceNumber > current.sequenceNumber ? prev : current
  );

  return replicateWithMaxSequenceNumber.sequenceNumber;
}

export default reducer;
