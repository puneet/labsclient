import axios from "axios";

export function fetchWorksheets() {
  return (dispatch: any) => {
    dispatch({
      type: "FETCH_WORKSHEETS",
      payload: axios.get("/worksheet-list.json"),
    });
  };
}

export function fetchWorksheet(id: string) {
  return (dispatch: any) => {
    dispatch({
      type: "FETCH_WORKSHEET",
      payload: axios.get(`/worksheet-${id}.json`),
    });
  };
}

export function clearWorksheet() {
  return (dispatch: any) => {
    dispatch({
      type: "CLEAR_WORKSHEET",
    });
  };
}
