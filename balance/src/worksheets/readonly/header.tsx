import React from "react";
import { Link } from "react-router-dom";
import { IWorksheet } from "./../../types/worksheet";

function Header(props: any) {
  const worksheet: IWorksheet = props.worksheet;
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <a className="navbar-item is-size-3 back-arrow" href="/">
          <span className="material-icons">arrow_back</span>
        </a>
        <div className="navbar-item is-size-4 has-text-white">
          {worksheet.name}
        </div>
      </div>

      <div className="navbar-end">
        <div className="navbar-item">
          <div className="buttons">
            <Link
              to={`/worksheets/${worksheet.id}/edit`}
              className="button is-primary"
            >
              Edit
            </Link>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Header;
