import React from "react";
import { IReplicate } from "./../../types/worksheet";

interface IProps {
  replicate: IReplicate;
}

function Replicate(props: IProps) {
  const { replicate } = props;
  return (
    <tr>
      <td>{replicate.name}</td>
      <td>{replicate.replicateWeight?.value}</td>
      <td>{replicate.istdWeight?.value}</td>
      <td>{replicate.theoreticalVolume?.value}</td>
      <td>{replicate.dilution?.value}</td>
      <td>{replicate.weighedOn}</td>
      <td>{replicate.weighedBy}</td>
      <td>{replicate.weightTransferredFromBalance ? "Balance" : "Manual"}</td>
    </tr>
  );
}

export default Replicate;
