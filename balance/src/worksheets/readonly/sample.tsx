import React from "react";
import { ISample } from "./../../types/worksheet";
import Replicates from "./replicates";

interface IProps {
	sample: ISample;
}

function Sample(props: IProps) {
	const { sample } = props;
	return (
		<section className="section">
			<p className="is-size-4">Barcode: {sample.barcode}</p>
			<div className="ml-5">
				<Replicates replicates={sample.replicates} />
			</div>
		</section>
	);
}

export default Sample;
