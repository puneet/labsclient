import React from "react";
import { ISample } from "./../../types/worksheet";
import Sample from "./sample";

interface IProps {
	samples: ISample[];
}

function Samples(props: IProps) {
	return (
		<>
			<h2 className="is-size-2">Samples</h2>
			{props.samples?.map((sample: ISample, i: number) => {
				return <Sample sample={sample} key={i} />;
			})}
		</>
	);
}

export default Samples;
