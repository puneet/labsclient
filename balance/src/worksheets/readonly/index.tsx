import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchWorksheet } from "./../dux/actions";
import { IWorksheet } from "./../../types/worksheet";
import { withRouter } from "react-router";

import Header from "./header";
import Samples from "./samples";
import Standards from "./standards";
import ProcessingMethodComponents from "./processing-method-components";
import Equipment from "./equipment";
import Solvents from "./solvents";
import Chemicals from "./chemicals";
import InternalStandardSolution from "./internal-standard-solution";

interface IProps {
	worksheet: IWorksheet;
	fetchWorksheet: (id: string) => void;
}

const ReadOnlyWorksheet = function(props: any) {
	const { worksheetId } = props.match.params;
	const { fetchWorksheet, worksheet } = props;

	useEffect(() => {
		fetchWorksheet(worksheetId);
	}, [fetchWorksheet, worksheetId]);

	if (!props.worksheet) {
		return null;
	}

	// @@@ Why is this rendered twice?
	return (
		<>
			<Header worksheet={worksheet} />
			<Samples samples={worksheet.samples} />
			<Standards standards={worksheet.standards} />
			<ProcessingMethodComponents />
			<Chemicals chemicals={worksheet.chemicals} />
			<Equipment equipments={worksheet.equipments} />
			<Solvents solvents={worksheet.solvents} />
			<InternalStandardSolution
				internalStandardSolution={worksheet.internalStandardSolution}
			/>
		</>
	);
};

function mapStateToProps(state: any) {
	return {
		worksheet: state.worksheetsState.worksheet
	};
}

const mapDispatchToProps = {
	fetchWorksheet
};

const ConnectedReadOnlyWorksheet = withRouter(
	connect(mapStateToProps, mapDispatchToProps)(ReadOnlyWorksheet)
);

export default ConnectedReadOnlyWorksheet;
