import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchWorksheets } from "./dux/actions";
import { IWorksheet } from "./../types/worksheet";
import { Link } from "react-router-dom";
import NavBar from "./navbar";

interface IProps {
  worksheets: IWorksheet[];
  fetchWorksheets: () => void;
}

const Worksheets = function (props: IProps) {
  useEffect(props.fetchWorksheets, []);

  if (!props.worksheets?.length) {
    return null;
  }

  return (
    <>
      <NavBar />
      <div className="container mt-3">
        <h1 className="is-size-1">Worksheets</h1>
        <table className="table is-fullwidth">
          <thead>
            <tr>
              <th>Name</th>
              <th>Created On</th>
              <th>Created By</th>
              <th>Last Updated On</th>
              <th>Last Updated By</th>
              <th>&nbsp;</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            {props.worksheets.map((worksheet: IWorksheet, i: number) => (
              <tr key={i}>
                <td>{worksheet.name}</td>
                <td>{worksheet.createdOn}</td>
                <td>{worksheet.createdBy}</td>
                <td>{worksheet.lastModifiedOn}</td>
                <td>{worksheet.lastModifiedBy}</td>
                <td>
                  <Link to={`worksheets/${worksheet.id}/view`}>View</Link>
                </td>
                <td>
                  <Link to={`worksheets/${worksheet.id}/edit`}>Edit</Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

function mapStateToProps(state: any) {
  return {
    worksheets: state.worksheetsState.worksheets,
  };
}

const mapDispatchToProps = {
  fetchWorksheets,
};

const ConnectedWorksheets = connect(
  mapStateToProps,
  mapDispatchToProps
)(Worksheets);

export default ConnectedWorksheets;
