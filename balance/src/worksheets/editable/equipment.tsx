import React from "react";
import { IEquipment } from "./../../types/worksheet";

interface IProps {
	equipments: IEquipment[];
}

function Equipment(props: IProps) {
	return props.equipments?.length ? (
		<section className="">
			<h2 className="is-size-2">Equipment</h2>
			<ul>
				{props.equipments.map((equipment: IEquipment, i: number) => {
					return <li key={i}>{equipment.barcode}</li>;
				})}
			</ul>
		</section>
	) : null;
}

export default Equipment;
