import React from "react";
import { IStandard } from "./../../types/worksheet";
import Standard from "./standard";

interface IProps {
	standards: IStandard[];
}

function Standards(props: IProps) {
	return (
		<>
			<h2 className="is-size-2">Reference Materials</h2>
			{props.standards.map((standard: IStandard, i: number) => {
				return <Standard standard={standard} key={i} />;
			})}
		</>
	);
}

export default Standards;
