import React from "react";
import { IChemical } from "./../../types/worksheet";

interface IProps {
	chemicals: IChemical[];
}

function Chemicals(props: IProps) {
	return props.chemicals.length ? (
		<>
			<h2>Chemicals</h2>
			<ul>
				{props.chemicals?.map((chemical: IChemical, i: number) => (
					<li key={i}>{chemical.name}</li>
				))}
			</ul>
		</>
	) : null;
}

export default Chemicals;
