import React from "react";
import { Route, Switch } from "react-router-dom";
import { IWorksheet } from "./../../types/worksheet";
import Chemicals from "./chemicals";
import Equipment from "./equipment";
import InternalStandardSolution from "./internal-standard-solution";
import Overview from "./overview";
import Samples from "./samples";
import Solvents from "./solvents";
import Standards from "./standards";

interface IProps {
  match: any;
  worksheet: IWorksheet;
}

function Routes(props: IProps) {
  const { match, worksheet } = props;
  return (
    <Switch>
      <Route
        exact
        path={`${match.path}`}
        render={() => <Overview {...props} />}
      />
      <Route
        path={`${match.path}/samples`}
        render={() => <Samples samples={worksheet.samples} {...props} />}
      />
      <Route
        path={`${match.path}/standards`}
        render={() => <Standards standards={worksheet.standards} {...props} />}
      />
      <Route
        path={`${match.path}/chemicals`}
        render={() => <Chemicals chemicals={worksheet.chemicals} {...props} />}
      />
      <Route
        path={`${match.path}/solvents`}
        render={() => <Solvents solvents={worksheet.solvents} {...props} />}
      />
      <Route
        path={`${match.path}/equipment`}
        render={() => (
          <Equipment equipments={worksheet.equipments} {...props} />
        )}
      />
      <Route
        path={`${match.path}/internal-standard-solution`}
        render={() => (
          <InternalStandardSolution
            internalStandardSolution={worksheet.internalStandardSolution}
          />
        )}
        {...props}
      />
    </Switch>
  );
}

export default Routes;
