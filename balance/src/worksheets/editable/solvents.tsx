import React from "react";
import { ISolvent } from "./../../types/worksheet";

interface IProps {
	solvents: ISolvent[];
}

function Solvents(props: IProps) {
	return props.solvents?.length ? (
		<>
			<h2 className="is-size-2">Solvents</h2>
			<ul>
				{props.solvents.map((solvent: ISolvent, i: number) => {
					return <li key={i}>{solvent.chemicalName}</li>;
				})}
			</ul>
		</>
	) : null;
}

export default Solvents;
