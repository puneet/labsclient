import React from "react";
import { NavLink } from "react-router-dom";

interface IProps {
  baseUrl: string;
}

function Tabs(props: IProps) {
  const baseUrl = props.baseUrl;
  const tabs = [
    { path: "overview", name: "Overview" },
    { path: "samples", name: "Samples" },
    { path: "standards", name: "Standards" },
    { path: "chemicals", name: "Cemicals" },
    { path: "solvents", name: "Solvents" },
    { path: "equipment", name: "Equipment" },
    {
      path: "internal-standard-solution",
      name: "Internal Standard Solution",
    },
  ];

  return (
    <div className="tabs">
      <ul>
        {tabs.map((tab, i) => (
          <li key={i}>
            <NavLink to={tab.path ? `${baseUrl}/${tab.path}` : `${baseUrl}`}>
              {tab.name}
            </NavLink>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Tabs;
