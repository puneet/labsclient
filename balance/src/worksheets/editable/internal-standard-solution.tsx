import React from "react";
import {
	ISolvent,
	IInternalStandard,
	IInternalStandardSolution
} from "./../../types/worksheet";

interface IProps {
	internalStandardSolution: IInternalStandardSolution;
}

function InternalStandardSolution(props: IProps) {
	const iss = props.internalStandardSolution;

	if (!iss) {
		return null;
	}

	return (
		<>
			<h2 className="is-size-2">Internal Standard Solution</h2>
			<p>{iss.id}</p>
			<p>{iss.datePrepared}</p>
			<p>{iss.internalStandard}</p>
			<p>{iss.reference}</p>
			<p>{iss.volume}</p>
			<p>{iss.weight}</p>
			<p>{iss.reference}</p>
			<p>
				<ul>
					{iss.solvents?.map((solvent: ISolvent, i: number) => (
						<li key={i}>{solvent.chemicalName}</li>
					))}
				</ul>
			</p>
			{renderInternalStandard(iss.internalStandard)}
		</>
	);

	function renderInternalStandard(is: IInternalStandard) {
		if (!is) {
			return;
		}

		return (
			<div>
				<p>{is.id}</p>
				<p>{is.chemicalName}</p>
				<p>{is.lotNumber}</p>
				<p>{is.manufacturer}</p>
			</div>
		);
	}
}

export default InternalStandardSolution;
