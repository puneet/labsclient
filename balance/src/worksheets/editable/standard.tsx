import React from "react";
import { IStandard } from "./../../types/worksheet";
import Replicates from "./replicates";

interface IProps {
	standard: IStandard;
}

function Standard(props: IProps) {
	const { standard } = props;
	return (
		<section className="section">
			<p className="is-size-4">Barcode: {standard.barcode}</p>
			<div className="ml-5">
				<Replicates replicates={standard.replicates} />
			</div>
		</section>
	);
}

export default Standard;
