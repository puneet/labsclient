import React from "react";
import { connect } from "react-redux";
import { ILookup } from "../../app/dux/reducer";
import { IReplicate } from "./../../types/worksheet";
import { rejectReplicate } from "./../dux/sample-actions";
import VolumeMeasurement from "./volume-measurement";
import WeightMeasurement from "./weight-measurement";

interface IProps {
  replicate: IReplicate;
  weightUnits: ILookup[];
  volumeUnits: ILookup[];
}

function Replicate(props: IProps) {
  const { replicate, weightUnits, volumeUnits } = props;

  function rejectReplicate(replicateId: string) {
    console.log(`Rejecting replicate: ${replicateId}`);
  }

  return (
    <tr>
      <td className="text-only-cell">{replicate.sequenceNumber}</td>
      <td>
        <WeightMeasurement
          weight={replicate.replicateWeight}
          weightUnits={weightUnits}
        />
      </td>
      <td>
        <WeightMeasurement
          weight={replicate.istdWeight}
          weightUnits={weightUnits}
        />
      </td>
      <td>
        <VolumeMeasurement
          volume={replicate.theoreticalVolume}
          volumeUnits={volumeUnits}
        />
      </td>
      <td>
        <VolumeMeasurement
          volume={replicate.dilution}
          volumeUnits={volumeUnits}
        />
      </td>
      <td>
        <button
          className="button"
          onClick={() => {
            rejectReplicate(replicate.sequenceNumber.toString());
          }}
        >
          x
        </button>
      </td>
    </tr>
  );
}

function mapStateToProps(state: any) {
  const appState = state.appState;
  return {
    weightUnits: appState.weightUnits,
    volumeUnits: appState.volumeUnits,
  };
}

const ConnectedReplicate = connect(mapStateToProps, { rejectReplicate })(
  Replicate
);

export default ConnectedReplicate;
