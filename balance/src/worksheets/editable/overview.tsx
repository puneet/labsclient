import React from "react";
import { IWorksheet } from "./../../types/worksheet";

interface IProps {
	worksheet: IWorksheet;
}

function WorksheetOverview(props: IProps) {
	const { worksheet } = props;
	return (
		<>
			<h1>Overview</h1>
			<p>Samples: {worksheet.samples?.length}</p>
		</>
	);
}

export default WorksheetOverview;
