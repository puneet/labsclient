import React from "react";
import { IReplicate } from "./../../types/worksheet";
import Replicate from "./replicate";

interface IProps {
  replicates: IReplicate[];
}

function Replicates(props: IProps) {
  return (
    <>
      <table className="table replicate-table">
        <thead>
          <tr>
            <th>Number</th>
            <th>Weight</th>
            <th>Istd Weight</th>
            <th>Theoretical Vol.</th>
            <th>Dilution</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {props.replicates?.map((replicate: IReplicate, i: number) => {
            return <Replicate replicate={replicate} key={i} {...props} />;
          })}
        </tbody>
      </table>
    </>
  );
}

export default Replicates;
