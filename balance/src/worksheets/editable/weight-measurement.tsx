import React, { useEffect, useState } from "react";
import { ILookup } from "../../app/dux/reducer";
import { IWeightMeasurement } from "../../types/worksheet";

interface IProps {
  weight?: IWeightMeasurement;
  weightUnits: ILookup[];
}

const WeightMeasurement: React.FC<IProps> = function (props: IProps) {
  const { weightUnits, weight } = props;
  const { value, unitId } = weight || {};

  const [weightValue, setWeightValue] = useState(value);
  const [weightUnitId, setWeightUnitId] = useState(unitId);

  useEffect(() => {
    setWeightValue(weight?.value);
    setWeightUnitId(weight?.unitId);
  }, [weight]);

  return (
    <div className="field has-addons">
      <div className="control" style={{ minWidth: "4rem" }}>
        <input
          type="number"
          value={weightValue || ""}
          onChange={(e) => setWeightValue(+e.target.value)}
          className="input is-medium"
        />
      </div>
      <div className="control">
        <div className="select is-medium" style={{ width: "6rem" }}>
          <select
            value={weightUnitId || ""}
            onChange={(e) => setWeightUnitId(+e.target.value)}
          >
            {weightUnits?.map((unit: any, i: number) => (
              <option key={i} value={unit.id}>
                {unit.name}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
};

export default WeightMeasurement;
