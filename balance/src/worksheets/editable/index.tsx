import React, { useEffect } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { IWorksheet } from "./../../types/worksheet";
import { clearWorksheet, fetchWorksheet } from "./../dux/actions";
import Header from "./header";
import "./index.scss";
import Routes from "./routes";
import Tabs from "./tabs";

interface IProps {
  match: any;
  worksheet: IWorksheet;
  fetchWorksheet: (id: string) => void;
  clearWorksheet: () => void;
}

const EditableWorksheet = function (props: IProps) {
  const { worksheetId } = props.match.params;
  const { match, fetchWorksheet, clearWorksheet, worksheet } = props;

  useEffect(() => {
    fetchWorksheet(worksheetId);
    return clearWorksheet;
  }, [worksheetId, fetchWorksheet, clearWorksheet]);

  if (!props.worksheet) {
    return null;
  }

  return (
    <>
      <Header worksheet={worksheet} />
      <div className="container mt-3">
        <Tabs baseUrl={match.url} />
        <Routes {...props} />
      </div>
    </>
  );
};

function mapStateToProps(state: any) {
  return {
    worksheet: state.worksheetsState.worksheet,
  };
}

const mapDispatchToProps = {
  fetchWorksheet,
  clearWorksheet,
};

const ConnectedReadOnlyWorksheet = withRouter(
  connect(mapStateToProps, mapDispatchToProps)(EditableWorksheet)
);

export default ConnectedReadOnlyWorksheet;
