import React from "react";
import { connect } from "react-redux";
import { NavLink, Route, withRouter } from "react-router-dom";
import { ISample } from "./../../types/worksheet";
import Sample from "./sample";

interface IProps {
  match?: any;
  samples: ISample[];
}

const Samples: React.FC<IProps> = function (props: IProps) {
  function renderSamplesMenu(samples: ISample[]) {
    return samples?.map((sample: ISample, i: number) => {
      return (
        <NavLink
          to={`${props.match.url}/${sample.barcode}`}
          key={i}
          className="mb-3"
        >
          <div className="box mb-3">
            <div className="is-size-3">{sample.barcode}</div>
            <div className="is-size-5">
              {sample.replicates?.length} Replicates
            </div>
          </div>
        </NavLink>
      );
    });
  }

  function onAddNewSampleButtonClick() {
    console.log("Add new sample");
  }

  return (
    <>
      <div className="columns mt-3">
        <div className="is-2 samples-list">
          <button
            className="box mb-3"
            onClick={onAddNewSampleButtonClick}
            style={{ width: "100%" }}
          >
            <div className="is-size-3 has-text-white">+ New</div>
          </button>
          {renderSamplesMenu(props.samples)}
        </div>
        <div className="is-10">
          <Route
            path={`${props.match.path}/:barcode`}
            render={() => {
              return <Sample {...props} />;
            }}
          />
        </div>
      </div>
    </>
  );
};

function mapStateToProps(state: any, ownProps: any) {
  return {};
}

const ConnectedSamples = withRouter(connect(mapStateToProps, {})(Samples));

export default ConnectedSamples;
