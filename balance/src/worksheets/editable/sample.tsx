import React, { useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { ISample } from "./../../types/worksheet";
import { addReplicate } from "./../dux/sample-actions";
import Replicates from "./replicates";

interface IStateProps {
  sample: ISample;
  match: any;
}

interface IDispatchProps {
  addReplicate: (worksheetId: string, sampleId: string) => void;
}

interface IContainerProps {}

type IProps = IStateProps & IDispatchProps;

const Sample: React.FC<any> = (props: IProps) => {
  const { worksheetId, barcode } = props.match.params;
  const { sample } = props;

  const [sampleBarcode, setSampleBarcode] = useState(sample.barcode);
  const [sampleName, setSampleName] = useState(sample.name);

  function sampleBarcodeBlur(e: any) {
    const newValue = e.target.value;
    const oldValue = sampleBarcode;
    if (newValue != oldValue) {
      console.log(`Barcode changed from ${oldValue} to ${newValue}`);
      setSampleBarcode(newValue);
    }
  }

  function sampleNameBlur(e: any) {
    const newValue = e.target.value;
    const oldValue = sampleName;
    if (newValue != oldValue) {
      console.log(`Sample name changed from ${oldValue} to ${newValue}`);
      console.log({
        worksheetId,
        sampleId: sample.id,
        oldValue,
        newValue,
        date: new Date(),
        by: "User", //@@@ Get User
      });
      setSampleName(newValue);
    }
  }

  if (!sample) {
    return null;
  }

  return (
    <section className="sample-section">
      <div className="columns">
        <div className="column is-2">
          <div className="field">
            <label className="label">Barcode</label>
            <div className="control">
              <input
                className="input"
                type="text"
                value={sample.barcode}
                placeholder="Barcode"
                onChange={(e) => console.log(e.target.value)}
                onBlur={sampleBarcodeBlur}
              />
            </div>
          </div>
        </div>
        <div className="column is-10">
          <div className="field">
            <label className="label">Name</label>
            <div className="control">
              <input
                className="input"
                type="text"
                value={sample.name}
                placeholder="Name"
                onBlur={sampleNameBlur}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="mt-3">
        <Replicates replicates={sample.replicates} />
      </div>
      <button
        className="button is-primary mt-5"
        onClick={() => props.addReplicate(worksheetId, barcode)}
      >
        + New Replicate
      </button>
    </section>
  );
};

function mapStateToProps(state: any, ownProps: any) {
  const { barcode } = ownProps.match.params;
  const samples: ISample[] = state.worksheetsState.worksheet.samples;
  const sample = samples.find((s) => s.barcode === barcode);
  return { sample };
}

const ConnectedSample = withRouter(
  connect(mapStateToProps, { addReplicate })(Sample)
);

export default ConnectedSample;
