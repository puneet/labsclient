import React from "react";
import { Link, NavLink } from "react-router-dom";
import { IWorksheet } from "./../../types/worksheet";

function Header(props: any) {
  const worksheet: IWorksheet = props.worksheet;
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <NavLink className="navbar-item is-size-3 back-arrow" to="/">
          <span className="material-icons">arrow_back</span>
        </NavLink>
        <div className="navbar-item is-size-4 has-text-white">
          {worksheet.name}
        </div>
      </div>

      <div className="navbar-end">
        <div className="navbar-item">
          <div className="buttons">
            <Link
              to={`/worksheets/${worksheet.id}/edit`}
              className="button is-primary"
            >
              Complete
            </Link>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Header;
