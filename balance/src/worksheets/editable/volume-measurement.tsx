import React, { useEffect, useState } from "react";

function VolumeMeasurement(props: any) {
  const { volumeUnits, volume } = props;
  const { value, unitId } = volume || {};

  const [volumeValue, setVolumeValue] = useState(value || "");
  const [volumeUnitId, setVolumeUnitId] = useState(unitId);

  useEffect(() => {
    setVolumeValue(volume?.value);
    setVolumeUnitId(volume?.unitId);
  }, [volume]);

  return (
    <div className="field has-addons">
      <div className="control" style={{ minWidth: "4rem" }}>
        <input
          type="number"
          value={volumeValue}
          onChange={(e) => setVolumeValue(e.target.value)}
          className="input is-medium"
        />
      </div>
      <div className="control">
        <div className="select is-medium" style={{ width: "6rem" }}>
          <select
            value={volumeUnitId}
            onChange={(e) => setVolumeUnitId(e.target.value)}
          >
            {volumeUnits.map((unit: any, i: number) => (
              <option key={i} value={unit.id}>
                {unit.name}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
}

export default VolumeMeasurement;
