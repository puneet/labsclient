import React from "react";

function NavBar() {
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <a className="navbar-item is-size-3 has-text-white" href="/">
          Balance App
        </a>
      </div>

      <div className="navbar-end">
        <div className="navbar-item">User Name Here</div>
      </div>
    </nav>
  );
}

export default NavBar;
