import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import BalanceApp from "./app/index";
import { Provider } from "react-redux";
import store from "./store";

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<BalanceApp />
		</Provider>
	</React.StrictMode>,
	document.getElementById("root")
);
