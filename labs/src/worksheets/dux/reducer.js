const defaultState = {
  worksheets: [],
};

const reducer = (state = defaultState, action = {}) => {
  switch (action.type) {
    case "FETCH_WORKSHEETS_FULFILLED": {
      return { ...state, worksheets: action.payload.data };
    }

    case "FETCH_WORKSHEET_FULFILLED": {
      return { ...state, worksheet: action.payload.data };
    }

    default:
      return state;
  }
};

export default reducer;
