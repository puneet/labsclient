import axios from "axios";

export function fetchWorksheets() {
  return (dispatch) => {
    dispatch({
      type: "FETCH_WORKSHEETS",
      payload: axios.get("./worksheet-list.json"),
    });
  };
}

export function fetchWorksheet(worksheetId) {
  return (dispatch) => {
    dispatch({
      type: "FETCH_WORKSHEET",
      payload: axios.get(`/worksheet-${worksheetId}.json`),
    });
  };
}
