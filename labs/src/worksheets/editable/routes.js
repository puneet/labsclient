import React from "react";

import { Router } from "@reach/router";

import Overview from "./index";
import Samples from "./samples/index";
import SingleSample from "./samples/sample";

function EditWorksheetRoutes() {
  return (
    <Router>
      <Overview path="/worksheets/:worksheetId/edit">
        <Samples path="samples"></Samples>
      </Overview>
    </Router>
  );
}

export default EditWorksheetRoutes;
