import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchWorksheet } from "./../dux/actions";
import { Link } from "@reach/router";

function Replicates(props) {
  return (
    <>
      <table className="table">
        <thead>
          <tr>
            <th></th>
            <th>Weight</th>
            <th>Volume</th>
            <th>Theoretical Volume</th>
            <th>ISTD Weight</th>
          </tr>
        </thead>
        <tbody>
          {(props.replicates || []).map((replicate, idx) => {
            return (
              <tr key={idx}>
                <td>{replicate.name}</td>
                <td>{replicate.replicateWeight.value}</td>
                <td>{replicate.dilution.value}</td>
                <td>{replicate.theoreticalVolume.value}</td>
                <td>{replicate.istdWeight.value}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, { fetchWorksheet })(Replicates);
