import React from "react";
import { connect } from "react-redux";
import { Link } from "@reach/router";
import NavLink from "./../../../shared/navlink";
import Replicates from "./replicates";

function Sample(props) {
  const sampleId = props.sampleId;

  if (sampleId && props.samples) {
    const sample = props.samples.find((s) => s.barcode == sampleId);

    return (
      <>
        <h1>Single Sample</h1>
        <input type="text" value={sample.barcode} className="input" />
        <Replicates replicates={sample.replicates} />
      </>
    );
  }

  return <h1>Awaiting samples {sampleId}</h1>;
}

function mapStateToProps(state) {
  return {
    samples: state.worksheetsState?.worksheet?.samples,
  };
}

export default connect(mapStateToProps, {})(Sample);
