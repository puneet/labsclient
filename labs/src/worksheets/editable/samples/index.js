import React from "react";
import { connect } from "react-redux";
import { Link } from "@reach/router";
import NavLink from "./../../../shared/navlink";
import Sample from "./sample";
import SingleSample from "./sample";
import { Router } from "@reach/router";

function Samples(props) {
  const sampleId = props.sampleId;

  if (props.samples?.length) {
    return (
      <>
        <div className="columns">
          <div className="column auto">
            <a className="box has-text-white">
              <div className="is-size-4">+ New Sample</div>
            </a>
            {renderSampleList(props.samples)}
          </div>
          <div className="column is-10">
            <Router>
              <SingleSample path="/samples/:sampleId" />
            </Router>
          </div>
        </div>
      </>
    );
  }

  function renderSampleList(samples) {
    return samples.map((sample, i) => (
      <Link className="box" to={`samples/${sample.barcode}`} key={i}>
        <div className="is-size-4">{sample.barcode}</div>
        <div>{sample.replicates?.length || 0} replicates</div>
      </Link>
    ));
  }

  return <h1>Awaiting samples</h1>;
}

function mapStateToProps(state) {
  return {
    samples: state.worksheetsState?.worksheet?.samples,
  };
}

export default connect(mapStateToProps, {})(Samples);
