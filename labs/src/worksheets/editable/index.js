import React, { useEffect } from "react";
import { fetchWorksheet } from "./../dux/actions";
import { connect } from "react-redux";
import { Link } from "@reach/router";
import NavLink from "./../../shared/navlink";
import Samples from "./samples";

function EditWorksheetTabs() {
  const tabs = [
    { url: "", label: "Overview" },
    { url: "samples", label: "Samples" },
    { url: "reference-materials", label: "Reference Material" },
    { url: "chemicals-and-solvents", label: "Chemicals and solvents" },
    { url: "equipment", label: "Equipment" },
  ];

  return (
    <div className="tabs">
      <ul>
        {tabs.map((tab, i) => (
          <li key={i}>
            <NavLink to={tab.url}>{tab.label}</NavLink>
          </li>
        ))}
      </ul>
    </div>
  );
}

function WorksheetEdit(props) {
  const worksheetId = props.worksheetId;

  useEffect(() => {
    props.fetchWorksheet(worksheetId);
  }, [worksheetId]);

  if (!props.worksheet) {
    return null;
  }

  return (
    <>
      <h1 className="is-size-1">Worksheet {worksheetId}</h1>
      <EditWorksheetTabs />
      <Samples samples={props.worksheet.samples} />
    </>
  );
}

function mapStateToProps(state) {
  return {
    worksheet: state.worksheetsState.worksheet,
  };
}

export default connect(mapStateToProps, { fetchWorksheet })(WorksheetEdit);
