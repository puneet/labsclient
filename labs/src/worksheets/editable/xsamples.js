import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchWorksheet } from "./../dux/actions";
import { Link } from "@reach/router";

import Replicates from "./replicates";

function Samples(props) {
  return (
    <>
      <div className="accordion mt-3" id="sampleAccordion">
        {props.samples.map((sample, idx) => {
          return (
            <div key={idx}>
              <div className="card">
                <div className="card-header" id={`sample-${idx}-heading`}>
                  <h2 className="mb-0">
                    <button
                      className="btn btn-link btn-block text-left"
                      type="button"
                      data-toggle="collapse"
                      data-target={`#sample-${idx}`}
                      aria-expanded="true"
                      aria-controls="collapseOne">
                      {sample.barcode}
                    </button>
                  </h2>
                </div>

                <div
                  id={`sample-${idx}`}
                  className="collapse show"
                  aria-labelledby={`sample-${idx}-heading`}
                  data-parent="#sampleAccordion">
                  <div className="card-body">
                    <Replicates replicates={sample.replicates} />
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, { fetchWorksheet })(Samples);
