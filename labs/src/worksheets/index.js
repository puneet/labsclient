import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchWorksheets } from "./dux/actions";
import { Link } from "@reach/router";

function WorksheetList(props) {
  useEffect(() => {
    props.fetchWorksheets();
  }, []);

  return (
    <>
      <nav className="level mb-1">
        <div className="level-left">
          <h1 className="is-size-1">Worksheet List</h1>
        </div>

        <div className="level-right">
          <Link to="/worksheets/new" className="button is-text">
            New Worksheet
          </Link>
        </div>
      </nav>

      <table className="table is-fullwidth">
        <thead>
          <tr>
            <th>Name</th>
            <th>&nbsp;</th>
            <th>Created On</th>
            <th>Last Updated By</th>
            <th>Last Updated On</th>
          </tr>
        </thead>
        <tbody>
          {props.worksheets.map((ws) => (
            <tr key={ws.id}>
              <td>
                <Link to={`./../worksheets/${ws.id}/edit`} className="">
                  {ws.name}
                </Link>
              </td>
              <td>
                <Link to={`./../worksheets/${ws.id}/view`} className="">
                  View
                </Link>
              </td>
              <td>{ws.createdOn}</td>
              <td>{ws.user}</td>
              <td>{ws.lastModifiedOn}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

function mapStateToProps(state) {
  return {
    worksheets: state.worksheetsState.worksheets,
  };
}

export default connect(mapStateToProps, { fetchWorksheets })(WorksheetList);
