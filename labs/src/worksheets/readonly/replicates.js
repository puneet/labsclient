import React from "react";

import Replicate from "./replicate";

function Replicates(props) {
  return (
    <>
      <table className="table">
        <thead>
          <tr>
            <th>Number</th>
            <th>Weight</th>
            <th>Istd Weight</th>
            <th>Theoretical Vol.</th>
            <th>Dilution</th>
            <th>Weighed On</th>
            <th>Weighed By</th>
            <th>Source of Weight</th>
          </tr>
        </thead>
        <tbody>
          {(props.replicates || []).map((replicate, i) => {
            return <Replicate replicate={replicate} key={i} />;
          })}
        </tbody>
      </table>
    </>
  );
}

export default Replicates;
