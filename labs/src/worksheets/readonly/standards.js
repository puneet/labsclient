import React from "react";

import Standard from "./standard";

function Standards(props) {
  return (
    <>
      <h2 className="is-size-2">Reference Materials</h2>
      {props.standards.map((standard, i) => {
        return <Standard standard={standard} key={i} />;
      })}
    </>
  );
}

export default Standards;
