import React from "react";

import Replicates from "./replicates";

function Sample(props) {
  const { sample } = props;
  return (
    <section className="section">
      <p className="is-size-4">Barcode: {sample.barcode}</p>
      <div className="ml-5">
        <Replicates replicates={sample.replicates} />
      </div>
    </section>
  );
}

export default Sample;
