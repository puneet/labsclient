import React from "react";

import Replicates from "./replicates";

function Standard(props) {
  const { standard } = props;
  return (
    <section className="section">
      <p className="is-size-4">Barcode: {standard.barcode}</p>
      <div className="ml-5">
        <Replicates replicates={standard.replicates} />
      </div>
    </section>
  );
}

export default Standard;
