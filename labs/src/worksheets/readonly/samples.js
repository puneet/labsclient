import React from "react";

import Sample from "./sample";

function Samples(props) {
  return (
    <>
      <h2 className="is-size-2">Samples</h2>
      {props.samples.map((sample, i) => {
        return <Sample sample={sample} key={i} />;
      })}
    </>
  );
}

export default Samples;
