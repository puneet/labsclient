import React from "react";

function Equipment(props) {
  return props.equipments.length ? (
    <section className="">
      <h2 className="is-size-2">Equipment</h2>
      <ul>
        {props.equipments.map((equipment, i) => {
          return <li key={i}>{equipment.barcode}</li>;
        })}
      </ul>
    </section>
  ) : null;
}

export default Equipment;
