import React from "react";
import { connect } from "react-redux";

import Header from "./header";
import Samples from "./samples";
import Standards from "./standards";
import ProcessingMethodComponents from "./processing-method-components";
import Equipment from "./equipment";
import Solvents from "./solvents";
import Chemicals from "./chemicals";
import InternalStandardSolution from "./internal-standard-solution";

import { fetchWorksheet } from "./../dux/actions";

function ReadOnlyWorksheet(props) {
  const worksheetId = props.worksheetId;

  const worksheet = props.worksheet || {};

  React.useEffect(() => {
    props.fetchWorksheet(worksheetId);
  }, [worksheetId]);

  if (!props.worksheet) {
    return null;
  }

  return (
    <>
      <h1>Read only worksheet</h1>
      <Header name={worksheet.name} />
      <Samples samples={worksheet.samples} />
      <Standards standards={worksheet.standards} />
      <ProcessingMethodComponents />
      <Chemicals chemicals={worksheet.chemicals} />
      <Equipment equipments={worksheet.equipments} />
      <Solvents solvents={worksheet.solvents} />
      <InternalStandardSolution />
    </>
  );
}

function mapStateToProps(state) {
  return {
    worksheet: state.worksheetsState.worksheet,
  };
}

ReadOnlyWorksheet = connect(mapStateToProps, { fetchWorksheet })(
  ReadOnlyWorksheet
);

export default ReadOnlyWorksheet;
