import React from "react";

function Solvents(props) {
  return props.solvents.length ? (
    <>
      <h2 className="is-size-2">Solvents</h2>
      <ul>
        {props.solvents.map((solvent, i) => {
          return <li key={i}>{solvent.chemicalName}</li>;
        })}
      </ul>
    </>
  ) : null;
}

export default Solvents;
