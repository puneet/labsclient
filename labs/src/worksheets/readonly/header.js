import React from "react";

function Header(props) {
  return (
    <>
      <h2>Header</h2>
      <p>{props.name}</p>
    </>
  );
}

export default Header;
