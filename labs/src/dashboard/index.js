import React from "react";

export default function () {
  return (
    <>
      <h1 className="is-size-1">Dashboard</h1>

      <div className="row align-items-start">
        <div className="col text-secondary">
          <section>
            <h2>Worksheeets / week</h2>
            <svg
              width="20em"
              height="20em"
              viewBox="0 0 16 16"
              className="bi bi-graph-up"
              fill="currentColor"
              xmlns="http://www.w3.org/2000/svg">
              <path d="M0 0h1v16H0V0zm1 15h15v1H1v-1z" />
              <path
                fillRule="evenodd"
                d="M14.39 4.312L10.041 9.75 7 6.707l-3.646 3.647-.708-.708L7 5.293 9.959 8.25l3.65-4.563.781.624z"
              />
              <path
                fillRule="evenodd"
                d="M10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V4h-3.5a.5.5 0 0 1-.5-.5z"
              />
            </svg>
          </section>
        </div>
        <div className="col text-secondary">
          <section>
            <h2>Worksheets / location</h2>
            <svg
              width="20em"
              height="20em"
              viewBox="0 0 16 16"
              className="bi bi-pie-chart"
              fill="currentColor"
              xmlns="http://www.w3.org/2000/svg">
              <path
                fillRule="evenodd"
                d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"
              />
              <path
                fillRule="evenodd"
                d="M7.5 7.793V1h1v6.5H15v1H8.207l-4.853 4.854-.708-.708L7.5 7.793z"
              />
            </svg>
          </section>
        </div>
      </div>
    </>
  );
}
