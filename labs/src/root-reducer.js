import { combineReducers } from "redux";
import appReducer from "./app/dux/reducer";
import worksheetsReducer from "./worksheets/dux/reducer";

const rootReducer = combineReducers({
  appState: appReducer,
  worksheetsState: worksheetsReducer,
});

export default rootReducer;
