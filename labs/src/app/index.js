import React from "react";
import { Router } from "@reach/router";
import NavBar from "./navbar";
import Dashboard from "./../dashboard";
import Worksheets from "./../worksheets";
import EditWorksheet from "./../worksheets/editable";
import ViewWorksheet from "./../worksheets/readonly";

import EditWorksheetRoutes from "./../worksheets/editable/routes";

function App() {
  return (
    <>
      <NavBar />
      <div className="container" style={{ marginTop: "5rem" }}>
        <Router>
          <Dashboard path="/" />
          <Worksheets path="worksheets" />
          <ViewWorksheet path="worksheets/:worksheetId/view" />
        </Router>
        <EditWorksheetRoutes />
      </div>
    </>
  );
}

export default App;
